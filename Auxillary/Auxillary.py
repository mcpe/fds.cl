###########
# Imports #
###########
import numpy
import pyopencl

#######################################################################
# Function for building p(r) distribution and Fourier transforming it #
#######################################################################
def FastDebyeSum(ListOfqValues, ProteinCoordinates, ScatteringLengthDictionary, NumberOfBins, OpenCLContext, OpenCLPrograms):
	# Set up OpenCL
	OpenCLQueue = pyopencl.CommandQueue(OpenCLContext)
	OpenCLCalculatePofr, OpenCLFourierTransform = OpenCLPrograms

	# Parameters for the point cloud and data
	NumberOfqValues = numpy.intc(len(ListOfqValues))
	qArray		    = numpy.array(ListOfqValues).astype(numpy.float32)
	IntensityArray  = numpy.zeros(NumberOfqValues).astype(numpy.float32)

	# Set up protein structure
	x, y, z, Atom = ProteinCoordinates
	NumberOfAtoms = numpy.intc(len(x))

	# Expand scattering dict
	ScatteringLengthArray       = [ScatteringLengthDictionary[Atom[i]] for i in range(NumberOfAtoms)]

	# Prepare arrays for p(r) calculation
	xArray = numpy.array(x).astype(numpy.float32)
	yArray = numpy.array(y).astype(numpy.float32)
	zArray = numpy.array(z).astype(numpy.float32)

	ScatteringLengthArray = numpy.array(ScatteringLengthArray).astype(numpy.float32)

	# High estimate of Dmax
	DMax      = ((xArray.max() - xArray.min())**2 + (yArray.max() - yArray.min())**2 + (zArray.max() - zArray.min())**2)**0.5
	RStepSize = DMax / NumberOfBins
	POfRArray = numpy.zeros(NumberOfBins).astype(numpy.float32)

	# Convert to buffers
	xBuffer                = pyopencl.Buffer(OpenCLContext, pyopencl.mem_flags.READ_ONLY  | pyopencl.mem_flags.COPY_HOST_PTR, hostbuf = xArray)
	yBuffer                = pyopencl.Buffer(OpenCLContext, pyopencl.mem_flags.READ_ONLY  | pyopencl.mem_flags.COPY_HOST_PTR, hostbuf = yArray)
	zBuffer                = pyopencl.Buffer(OpenCLContext, pyopencl.mem_flags.READ_ONLY  | pyopencl.mem_flags.COPY_HOST_PTR, hostbuf = zArray)
	ScatteringLengthBuffer = pyopencl.Buffer(OpenCLContext, pyopencl.mem_flags.READ_ONLY  | pyopencl.mem_flags.COPY_HOST_PTR, hostbuf = ScatteringLengthArray)
	PofrBuffer			   = pyopencl.Buffer(OpenCLContext, pyopencl.mem_flags.READ_WRITE | pyopencl.mem_flags.COPY_HOST_PTR, hostbuf = POfRArray)

	# Run kernel
	Kernel = OpenCLCalculatePofr.CalculatePofr(OpenCLQueue, (NumberOfAtoms, NumberOfAtoms), None, xBuffer, yBuffer, zBuffer, ScatteringLengthBuffer, numpy.float32(1.0 / RStepSize), PofrBuffer)
	Kernel.wait()
	pyopencl.enqueue_copy(OpenCLQueue, POfRArray, PofrBuffer)

	# Release buffers
	xBuffer.release()
	yBuffer.release()
	zBuffer.release()
	ScatteringLengthBuffer.release()
	PofrBuffer.release()

	# Prepare buffers for intensity calculation
	PofrBuffer	    = pyopencl.Buffer(OpenCLContext, pyopencl.mem_flags.READ_ONLY  | pyopencl.mem_flags.COPY_HOST_PTR, hostbuf = POfRArray)
	qBuffer		    = pyopencl.Buffer(OpenCLContext, pyopencl.mem_flags.READ_ONLY  | pyopencl.mem_flags.COPY_HOST_PTR, hostbuf = qArray)
	IntensityBuffer = pyopencl.Buffer(OpenCLContext, pyopencl.mem_flags.WRITE_ONLY | pyopencl.mem_flags.COPY_HOST_PTR, hostbuf = IntensityArray)

	# Run kernel
	Kernel = OpenCLFourierTransform.FourierTransform(OpenCLQueue, (NumberOfqValues, ), None, PofrBuffer, numpy.float32(RStepSize), numpy.intc(NumberOfBins), qBuffer, IntensityBuffer)
	#Kernel = OpenCLFourierTransform.FourierTransform(OpenCLQueue, (NumberOfqValues, NumberOfBins), None, PofrBuffer, numpy.float32(RStepSize), qBuffer, IntensityBuffer)
	Kernel.wait()
	pyopencl.enqueue_copy(OpenCLQueue, IntensityArray, IntensityBuffer)

	# Release buffers
	PofrBuffer.release()
	qBuffer.release()
	IntensityBuffer.release()

	# Return
	return RStepSize, POfRArray, IntensityArray
