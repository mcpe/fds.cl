###########
# Imports #
###########
import pyopencl
import sys

#########################################
# Code for setting up an OpenCL context #
#########################################
def CreateOpenCLContext(ID):
	Platforms     = pyopencl.get_platforms()
	Platform      = Platforms[ID]
	Device        = Platform.get_devices()
	OpenCLContext = pyopencl.Context(Device)

	sys.stdout.write("\nRunning OpenCL on: \n" + str(Device) + ".\n\n")

	return OpenCLContext

####################################
# Code for building OpenCL kernels #
####################################
def BuildOpenCL(ListOfSources, OpenCLContext):
	CompiledPrograms = []

	for Program in ListOfSources:
		CompiledPrograms.append(pyopencl.Program(OpenCLContext, open(Program).read()).build())
	
	return CompiledPrograms
