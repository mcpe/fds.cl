//////////////////////////////////////////////////
// Function for atomic float addition in OpenCL //
//////////////////////////////////////////////////
inline void AtomicFloatAddition(volatile __global float *Destination, float Value)
{
	union{
		unsigned int ValueAsInt;
		float        ValueAsFloat;
	} Next, Expected, Current;

	Current.ValueAsFloat = *Destination;

	do{
		Expected.ValueAsFloat = Current.ValueAsFloat;
		Next.ValueAsFloat     = Expected.ValueAsFloat + Value;
		Current.ValueAsInt    = atomic_cmpxchg((volatile __global unsigned int *) Destination, Expected.ValueAsInt, Next.ValueAsInt);
	} while(Current.ValueAsInt != Expected.ValueAsInt);
}

///////////////////////////////////////////////////////
// Function for calculating of p(r) from point cloud //
///////////////////////////////////////////////////////
__kernel void CalculatePofr(global const float *xBuffer, 
                            global const float *yBuffer, 
                            global const float *zBuffer, 
                            global const float *ScatteringLengthsBuffer, 
							const float InverseRStepSize, 
                            volatile global float *POfRBuffer)
{
	// Declarations
 	const int i = get_global_id(0);
 	const int j = get_global_id(1);

	// Discard half of the correlations
	if (j > i) {
		return;
	}

	// Self-correlations
	if (j == i) {
		AtomicFloatAddition(&POfRBuffer[0], ScatteringLengthsBuffer[i] * ScatteringLengthsBuffer[j]);

		return;
	}

	// Correlations
	local float Radius;

	Radius = fast_distance((float3) (xBuffer[i], yBuffer[i], zBuffer[i]), (float3) (xBuffer[j], yBuffer[j], zBuffer[j]));

	// Atomic addition of p(r)
	AtomicFloatAddition(&POfRBuffer[(int) floor(Radius * InverseRStepSize)], 2.0 * ScatteringLengthsBuffer[i] * ScatteringLengthsBuffer[j]);

	return;
}
