//////////////////////////////////////////////////
// Function for atomic float addition in OpenCL //
//////////////////////////////////////////////////
inline void AtomicFloatAddition(volatile __global float *Destination, float Value)
{
	union{
		unsigned int ValueAsInt;
		float        ValueAsFloat;
	} Next, Expected, Current;

	Current.ValueAsFloat = *Destination;

	do{
		Expected.ValueAsFloat = Current.ValueAsFloat;
		Next.ValueAsFloat     = Expected.ValueAsFloat + Value;
		Current.ValueAsInt    = atomic_cmpxchg((volatile __global unsigned int *) Destination, Expected.ValueAsInt, Next.ValueAsInt);
	} while(Current.ValueAsInt != Expected.ValueAsInt);
}

////////////////////////////////////////////
// Function for Fourier transforming p(r) //
////////////////////////////////////////////
//__kernel void FourierTransform(global const float *PofrBuffer,
//                               const float RStepSize,
//                               global const float *qBuffer,
//                               volatile global float *IntensityBuffer) 
//{
//	// Declarations
// 	const int DatapointID = get_global_id(0);
// 	const int BinID = get_global_id(1);
//	const float q = qBuffer[DatapointID];

//	local float Contribution;
//	local float Argument;

//	// Sum
//	if (BinID == 0) {
//		Contribution = PofrBuffer[BinID];

//	} else {
//		Argument = q * RStepSize * BinID;
//		Contribution = PofrBuffer[BinID] * native_divide(native_sin(Argument), Argument);
//    }

//	AtomicFloatAddition(&IntensityBuffer[DatapointID], 4.0 * M_PI_F * RStepSize * Contribution);

//	return;
//}

////////////////////////////////////////////
// Function for Fourier transforming p(r) //
////////////////////////////////////////////
__kernel void FourierTransform(global const float *PofrBuffer,
                               const float RStepSize,
                               const int NumberOfBins,
                               global const float *qBuffer,
                               global float *IntensityBuffer) 
{
	// Declarations
 	const int DatapointID = get_global_id(0);
	const float q = qBuffer[DatapointID];

	local int i;
	local float Sum;
	local float Argument;

	// Sum - remember to weigh the first bin by half
	Sum = PofrBuffer[0];

	for (i = 1; i < NumberOfBins; ++i) {
		Argument = q * RStepSize * i;

		Sum = fma(PofrBuffer[i], native_divide(native_sin(Argument), Argument), Sum);
	}
    
	IntensityBuffer[DatapointID] = 16.0 * M_PI_F * M_PI_F * Sum * RStepSize;

	return;
}
