###########
# Imports #
###########
import re

#######################################
# Function for importing a list of qs #
#######################################
def ImportQRange(Filename):
	qArray = []

	File = open(Filename)
	Data = File.readlines()
	File.close()

	for Line in Data:
		RegularExpression = r"\W*[-+]?(?:(?:\d*\.\d+)|(?:\d+\.?))(?:[Ee][+-]?\d+)?"
		Pattern = re.compile(RegularExpression, re.VERBOSE)
		Entries = Pattern.findall(Line)

		qArray.append(float(Entries[0]))		

	return qArray
	
###########################################################
# Function for importing atomic coordinates from PDB file #
###########################################################
def ImportPDB(Filename):
	x    = []
	y    = []
	z    = []
	Atom = []

	File = open(Filename)
	Data = File.readlines()
	File.close()

	for Line in Data:

		if Line[0:4] == "ATOM":
			x.append(float(Line[30:38]))
			y.append(float(Line[38:46]))
			z.append(float(Line[46:54]))
			Atom.append(Line[76:78].lstrip().rstrip())

	return x, y, z, Atom
	
##############################################################
# Function for generating a dictionary of scattering lengths #
##############################################################
def ImportScatteringDictionary(Filename):
	Dictionary = {}

	File = open(Filename)
	Data = File.readlines()
	File.close()

	for Line in Data:

		if Line[0] != "#":
			Atom, ScatteringLength = Line[0:3].lstrip().rstrip(), float(Line[3:])
			Dictionary[Atom] = ScatteringLength

	return Dictionary
	
#####################################
# Function for outputting intensity #
#####################################
def OutputIntensity(ListOfqValues, ListOfIntensityValues, Filename):
	File = open(Filename, "w")

	for i in range(len(ListOfqValues)):
		File.write("{:20E} {:20E}\n".format(ListOfqValues[i], ListOfIntensityValues[i]))

	File.close()
	
#######################################################
# Function for outputting pair distance distributions #
#######################################################
def OutputPairDistanceDistribution(RStepSize, ListOfPOfrValues, Filename):
	File = open(Filename, "w")

	for i in range(len(ListOfPOfrValues)):
		File.write("{:20E} {:20E}\n".format(i * RStepSize, ListOfPOfrValues[i]))

	File.close()
