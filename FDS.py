#!/usr/bin/env python

###########
# Imports #
###########
import argparse
import sys
import os

try:
	import pyopencl
except:
	sys.stderr.write("Unable to import the PyOpenCL module. Is it correctly installed?\n")
	sys.exit(-1)

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), "Auxillary"))
import Auxillary
import AuxillaryOpenCL
import IO

########
# Main #
########
if __name__ == "__main__":
	# Assign CLI arguments
	Parser = argparse.ArgumentParser(description = "Software for calculating small-angle scattering intensities from .pdb-files using a Fast Debye sum coded in  the PyOpenCL framework.")
	Parser.add_argument("p", metavar = "Path to .pdb-file", help = "Path to atomic coordinates in .pdb-format")

	# Optional arguments
	Parser.add_argument("-d", metavar = "", help = "Path to datafile, default is Example/DefaultData.dat"                          , default = os.path.join(os.path.dirname(os.path.abspath(__file__)), "Example/DefaultData.dat"))
	Parser.add_argument("-s", metavar = "", help = "Path to dictionary of scattering lengths, default is ScatteringLengths/SAXS.sd", default = os.path.join(os.path.dirname(os.path.abspath(__file__)), "ScatteringLengths/SAXS.sd"))
	Parser.add_argument("-b", metavar = "", help = "Number of bins in p(r) distribution, default is 10000"                         , default = "10000")
	Parser.add_argument("-o", metavar = "", help = "ID of device on which to run OpenCL code, default is 0"                        , default = "0")
	Parser.add_argument("-v", metavar = "", help = "If set to 1, the program prints progress to stdout, default is 0"              , default = "0")
	Parser.add_argument("-w", metavar = "", help = "If set to 1, the program outputs the p(r) distribution, default is 0"          , default = "0")

	# Extract parameters
	Arguments = Parser.parse_args()

	PathToDatafile             = Arguments.d
	PathToPDBFile              = Arguments.p 
	PathToScatteringDictionary = Arguments.s
	DeviceID                   = int(Arguments.o)
	NumberOfBins               = int(Arguments.b)
	VerboseMode                = bool(int(Arguments.v))
	OutputPOfR                 = bool(int(Arguments.w))

	# Set verbose printing
	PrintToStdout = sys.stdout.write if VerboseMode else lambda *Arguments: None

	# Set up GPU and PyOpenCL programs
	Platforms = pyopencl.get_platforms()

	if not Platforms:
		sys.stderr.write("\nNo OpenCL-compatible devices found. Exiting.\n\n")
		sys.exit(-1)

	Platform      = Platforms[DeviceID]
	Device        = Platform.get_devices()
	OpenCLContext = pyopencl.Context(Device)

	PrintToStdout("\nRunning OpenCL on: \n" + str(Device) + ".\n\n")

	# Build OpenCL kernels
	OpenCLPrograms = AuxillaryOpenCL.BuildOpenCL([os.path.join(os.path.dirname(os.path.abspath(__file__)), "Auxillary/CalculatePofr.cl"), os.path.join(os.path.dirname(os.path.abspath(__file__)), "Auxillary/FourierTransform.cl")], OpenCLContext)

	# Import data, atomic coordinates, and scattering properties
	ListOfqValues = IO.ImportQRange(PathToDatafile)
	PrintToStdout("Imported " + str(len(ListOfqValues)) + " datapoints from " + PathToDatafile + ".\n")

	ProteinCoordinates = IO.ImportPDB(PathToPDBFile)
	PrintToStdout("Imported " + str(len(ProteinCoordinates[0])) + " atomic coordinates from " + PathToPDBFile + ".\n")

	ScatteringDictionary = IO.ImportScatteringDictionary(PathToScatteringDictionary)
	PrintToStdout("Imported " + str(len(ScatteringDictionary)) + " scattering properties from " + PathToScatteringDictionary + ".\n")

	# Run algorithm
	RStepSize, ListOfPOfrValues, ListOfIntensityValues = Auxillary.FastDebyeSum(ListOfqValues, ProteinCoordinates, ScatteringDictionary, NumberOfBins, OpenCLContext, OpenCLPrograms)

	# Output
	PathToOutputFiles = os.path.splitext(PathToPDBFile)[0]

	if OutputPOfR:
		IO.OutputPairDistanceDistribution(RStepSize, ListOfPOfrValues, PathToOutputFiles + ".por")
		PrintToStdout("Pair distance distribution printed to " + PathToOutputFiles + ".por.\n")

	IO.OutputIntensity(ListOfqValues, ListOfIntensityValues, PathToOutputFiles + ".ioq")
	PrintToStdout("Intensity profile printed to " + PathToOutputFiles + ".ioq.\n")
