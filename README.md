# FDS.CL - A PyOpenCL implementation of the Fast Debye Sum

Copyright 2022, University of Copenhagen

Martin Cramer Pedersen, mcpe@nbi.ku.dk

This file is part of FDS.CL.

FDS.CL is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

FDS.CL is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with FDS.CL. If not, see <http://www.gnu.org/licenses/>.

If you use FDS.CL in your work, please use the following reference:
```
Pedersen, Johansen, Roche, Järva, Törnroth-Horsefield, & Arleth (2022) 
Refining structural models of membrane proteins with disordered domains in phospholipid nanodiscs
https://www.biorxiv.org/content/10.1101/2022.10.28.512841v1
```

## Table of Contents

 - Dependencies
 - Command line arguments
 - Example

## Dependencies

The software relies on the Python module PyOpenCL:

	https://documen.tician.de/pyopencl/

Accordingly, the software will only run on OpenCL-compatible devices.

## Command line arguments

The full list of optional command line arguments is as follows:

| Argument | Description                                                                    | Default                   |
| -------- | ------------------------------------------------------------------------------ | ------------------------- |
| -d       | Path to datafile, default is Example/DefaultData.dat                           | Example/DefaultData.dat   |
| -s       | Path to dictionary of scattering lengths, default is ScatteringLengths/SAXS.sd | ScatteringLengths/SAXS.sd |
| -b       | Number of bins in p(r) distribution, default is 10000                          | 10000                     |
| -o       | ID of device on which to run OpenCL code, default is 0                         | 0                         |
| -v       | If set to 1, the program prints progress to stdout, default is 0               | 0                         |
| -w       | If set to 1, the program outputs the p(r) distribution, default is 0           | 0                         |

## Example

As input, the software uses a structure file in .pdb-format. To run the program on e.g. the 2lyz structure of Lysozyme, type into the command line:

	python FDS.py 2lyz.pdb
	
which will produce the SAXS intensity, as this is the default set of scattering lengths used in the calculation.
